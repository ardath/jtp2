package pl.agh.jtp2.l4;

import java.io.File;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

public class Laboratorium4a_z3 {

	public static void main(String[] args) {
		if (noArgsGiven(args)) {
			notifyAndExit(DialogMessages.NO_ARGUMENT_GIVEN);
		} else {
			classFilePath = args[0];
		}

		ClassFileAnalyzer cfa = new ClassFileAnalyzer();
		cfa.setClassFile(new File(classFilePath));
		cfa.setClassName("pl.agh.jtp2.l4.Const");

		classFields = cfa.getDeclaredFields();
		Map<String, String> mapOfFields = cfa
				.convertListOfFieldsToMap(classFields);

		PropertiesFileCreator pfc = new PropertiesFileCreator();
		pfc.setProperties(mapOfFields);
		String outputFileName = "example.properties";
		if (pfc.savePropertiesToFile(outputFileName)) {
			System.out.println(DialogMessages.GREAT_SUCCESS + outputFileName);
		} else {
			System.out.println(DialogMessages.FAIL);
		}

	}

	private static void notifyAndExit(String message) {
		System.out.println(message);
		System.exit(1);
	}

	static boolean noArgsGiven(String[] args) {
		return args.length == 0;
	}

	private static String classFilePath;
	private static List<Field> classFields;

}
