package pl.agh.jtp2.l4;

public class DialogMessages {
	public static final String GREAT_SUCCESS = "It works!(TM) Dane zapisano do pliku ";
	public static final String FAIL = "Błąd. Nie udało się zapisać danych do pliku.";
	public static final String NO_ARGUMENT_GIVEN = "Podaj ścieżkę do pliku z klasą jako argument linii poleceń żeby uruchomić program.";

}
