package pl.agh.jtp2.l4;

import java.awt.Image;
import java.awt.image.BufferedImage;

public interface ImageFilter {
	Image process();
}
