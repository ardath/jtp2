package pl.agh.jtp2.l4;

import java.lang.reflect.Field;

public class Laboratorium4a_z4 {

	public static void main(String[] args) {

		/*
		 * Zdefiniuj finalną klasę posiadającą prywatne pole klasy ImageFilter
		 * oraz konstruktor inicujujący to pole obiektem klasy MeanFilter.
		 */
		MeanFilter originalFilter = new MeanFilter();
		MedianFilter filterSetWithReflection = new MedianFilter();
		ContainsImageFilter obj = new ContainsImageFilter(originalFilter);

		/*
		 * Korzystając z refleksji nadpisz pole prywatne obiektem innej klasy.
		 */
		Class<?> c = obj.getClass();
		try {
			Field filterField = c.getDeclaredField("filter");

			// Konieczne żeby uzyskać dostęp do pola prywatnego:
			filterField.setAccessible(true);

			try {
				// Ustaw nową wartość prywatnego pola na obiekcie:
				filterField.set(obj, filterSetWithReflection);

			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}

		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
	}
}
