package pl.agh.jtp2.l4;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class PropertiesFileCreator {

	public boolean savePropertiesToFile(String fileName) {
		boolean status = false;
		Iterator<Entry<String, String>> it = getProperties().entrySet()
				.iterator();
		File file = createFile(fileName);

		FileWriter fw;
		try {
			fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			while (it.hasNext()) {
				Entry<String, String> property = it.next();
				String line = createPropertyLine(property);
				bw.write(line);
				it.remove();
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		status = true;
		return status;

	}

	private String createPropertyLine(Entry<String, String> property) {
		return property.getKey() + " = " + property.getValue() + "\n";
	}

	private File createFile(String name) {
		File file = new File(name);

		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return file;
	}

	public Map<String, String> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, String> mapOfFields) {
		this.properties = mapOfFields;
	}

	private String fileName;
	private Map<String, String> properties;

}
