package pl.agh.jtp2.l4;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

public class MedianFilter implements ImageFilter {

	public Image process() {
		System.out.println("process()");
		int width = getOriginalImage().getWidth();
		int height = getOriginalImage().getHeight();
		int windowSize = getWindowSize();

		/* TODO: comment
		 */
        int windowPixels[];

        /* for each pixel: 
         * 		1. create filter window of given size
         * 		2. calculate grayscale values for window's pixels
         * 		3. calculate median value 
         * 		4. store median in new image
         */
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				windowPixels = createWindowForPixel(windowSize, i, j, width, height);

			}
		}

		return null;
	}


	private int[] createWindowForPixel(int windowSize, int i, int j, int width, int height) {
		int pixels[] = null;
		
		int xMin, xMax, yMin, yMax;

		return pixels;
	}

	public void setImage(BufferedImage img) {
		this.originalImage = img;
	}

	public BufferedImage getOriginalImage() {
		return originalImage;
	}

	public int getWindowSize() {
		return windowSize;
	}

	public void setWindowSize(int windowSize) {
		this.windowSize = windowSize;
	}

	private BufferedImage originalImage;
	private int windowSize;
}
