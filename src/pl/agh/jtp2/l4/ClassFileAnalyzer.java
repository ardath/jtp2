package pl.agh.jtp2.l4;

import java.io.File;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClassFileAnalyzer {

	public List<Field> getDeclaredFields() {
		URL url;
		Field[] fields = null;
		List<Field> listOfFields = null;
		try {
			url = getClassFile().toURI().toURL();
			URL[] urls = new URL[] { url };
			URLClassLoader cl = URLClassLoader.newInstance(urls);

			Class<?> c = cl.loadClass(getClassName());
			fields = c.getDeclaredFields();
			listOfFields = new ArrayList<Field>(Arrays.asList(fields));

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		return listOfFields;
	}

	public Map<String, String> convertListOfFieldsToMap(List<Field> classFields) {
		Map<String, String> mapOfFields = new HashMap<>();
		for (Field f : classFields) {
			String name = f.getName();
			String value = null;
			try {
				value = (String) f.get(f);
				mapOfFields.put(name, value);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return mapOfFields;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String name) {
		className = name;
	}

	public File getClassFile() {
		return classFile;
	}

	public void setClassFile(File file) {
		this.classFile = file;
	}

	private File classFile;
	private String className;

}
