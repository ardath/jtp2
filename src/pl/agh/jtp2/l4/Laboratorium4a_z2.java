package pl.agh.jtp2.l4;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLClassLoader;

import javax.imageio.ImageIO;

public class Laboratorium4a_z2 {
	public static void main(String[] args) throws IOException {
		String workingdirectory = System.getProperty("user.dir");
		System.out.println(workingdirectory);

		File imageFile = null;
		imageFile = new File("/home/ardath/dev/workspace/jtp2/extra/git-branching-model.png");
		imageFile = new File(workingdirectory, "extra/git-branching-model.png");
		String path = args[0];
		imageFile = new File(path);

		BufferedImage image = ImageIO.read(imageFile);
		MedianFilter mf = new MedianFilter();

		mf.setImage(image);
		mf.setWindowSize(3);
		mf.process();

		File file = new File("/home/ardath/dev/eclipse/jtp2/plugins/");
		URL url = file.toURI().toURL();
		URL[] urls = new URL[] { url };
		URLClassLoader cl = URLClassLoader.newInstance(urls);

		try {
			Class<?> c = cl.loadClass("pl.agh.jtp2.l4.MedianFilter");

			Type[] interfaces = c.getGenericInterfaces();
			for (Type i : interfaces) {
				System.out.println(i.toString());
			}

			Method[] methods = c.getMethods();
			for (Method m : methods) {
				System.out.println(m.toString());
			}

			Object obj = c.newInstance();

			Class<?>[] parametersForSetImage = new Class<?>[1];
			parametersForSetImage[0] = BufferedImage.class;
			Method setImage = c.getDeclaredMethod("setImage",
					parametersForSetImage);
			setImage.invoke(obj, image);

			Method process = c.getDeclaredMethod("process");
			process.invoke(obj);

			BufferedImage i = ((MedianFilter) obj).getOriginalImage();
			System.out.println(i.getClass());
			System.out.println(i.getWidth());
			System.out.println(i.getHeight());

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
