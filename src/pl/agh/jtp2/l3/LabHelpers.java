package pl.agh.jtp2.l3;

import java.util.ArrayList;
import java.util.List;

public class LabHelpers {
	public static List<String> generateAcceptedExtensionsList() {
		List<String> output = new ArrayList<>();
		output.add("html");
		output.add("mht");
		output.add("txt");
		output.add("csv");
		output.add("asc");
		return output;
	}


}
