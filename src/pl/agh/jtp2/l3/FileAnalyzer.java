package pl.agh.jtp2.l3;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

public class FileAnalyzer implements Runnable {

	private String searchString;
	private File file;
	private org.apache.logging.log4j.Logger logger;
	private List<File> foundFiles;

	public FileAnalyzer(String searchString, File file, List<File> foundFiles) {
		this.searchString = searchString;
		this.file = file;
		this.logger = LogManager.getRootLogger();
		this.foundFiles = foundFiles;
	}

	@Override
	public void run() {
		if (containsString(searchString, file)) {
			getLogger().info("Search string found in file. ");
			// System.out .println("Search string found in file " +
			// file.toString());
			foundFiles.add(file);
			// System.out.println(foundFiles.toString());
			// try { Thread.sleep((int) (Math.random() * 1000));
			// System.out.println("Sleeping with "+file.toString()); } catch
			// (InterruptedException e) { }
		} else {
			getLogger().info("Search string not found in file. ");
			// System.out.println("Search string not found in file "+file.toString());
		}

	}

	private boolean containsString(String s, File f) {
		try {
			String fc = FileUtils.readFileToString(f);
			if (fc.contains(s)) {
				return true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public org.apache.logging.log4j.Logger getLogger() {
		return this.logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

}
