package pl.agh.jtp2.l3;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;

public class Laboratorium3 {
	public static void main(String[] args) throws IOException {
		final org.apache.logging.log4j.Logger logger = LogManager
				.getRootLogger();

		Path searchDir = Paths.get("/home/ardath/dropbox/html");
		final String keywords = "Wikipedia, the free";
		//final String keywords = "java";
		final List acceptedExtensions = LabHelpers
				.generateAcceptedExtensionsList();

		int nThreads = 4;
		final ExecutorService es = Executors.newFixedThreadPool(nThreads);
		
		final List<File> foundFiles = Collections.synchronizedList(new ArrayList<File>());

		Files.walkFileTree(searchDir, new FileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file,
					BasicFileAttributes attrs) throws IOException {
				logger.info("Visiting file " + file.toString() + ". ");
				//System.out.println("Visiting file " + file.toString() + ". ");

				String fileExtension = FilenameUtils.getExtension(file
						.toString());
				if (acceptedExtensions.contains(fileExtension)) {
                    //System.out.println("Analysing file " + file.toString() + ". ");
					Runnable process = new FileAnalyzer(keywords, file.toFile(), foundFiles	);
//					Future<?> foundFile = es.submit(process);
					es.execute(process);
				} 
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc)
					throws IOException {
				logger.warn("Failed to visit file " + file.toString() + ". ");
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc)
					throws IOException {
				logger.warn("Finished visiting directory " + dir.toString()
						+ ". ");
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult preVisitDirectory(Path dir,
					BasicFileAttributes attrs) throws IOException {
				logger.warn("Searching in directory " + dir.toString() + ". ");
				return FileVisitResult.CONTINUE;
			}

		});

		es.shutdown();
		try {
			es.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        System.out.println("Listing found files with searched string: ");
		for(File f: foundFiles){
			System.out.println(f.toString());
		}
	};
}