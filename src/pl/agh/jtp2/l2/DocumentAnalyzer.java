package pl.agh.jtp2.l2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

class DocumentAnalyzer {

	DocumentAnalyzer() {
	}

	DocumentAnalyzer readTextFile(File f) {
		this.inputFile = f;
		this.fileContents = new ArrayList<String>();

		try (BufferedReader br = new BufferedReader(new FileReader(
				this.inputFile))) {
			for (String line; (line = br.readLine()) != null;) {
				fileContents.add(line);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Error: no such file. ");
		} catch (IOException e) {
			System.out.println("Error: IO error. ");
			e.printStackTrace();
		}

		return this;
	}

	DocumentAnalyzer printFileContents() {
		System.out.println("Printing file contents.. ");
		try {
			for (String line : this.fileContents) {
				System.out.println(line);
			}
		} catch (NullPointerException e) {
			System.out
					.println("Error: nothing to print. Read text file with .readTextFile() first.");
			// e.printStackTrace();
		}

		return this;
	}

	DocumentAnalyzer printAllWordsWithCounts() {
		try {
			System.out.println("Printing all words with their counts.. ");
			for (Map.Entry<String, Integer> me : this.counter.entrySet()) {
				String s = "word: " + me.getKey() + ", count: " + me.getValue()
						+ "; \n";
				System.out.println(s);
			}
		} catch (NullPointerException e) {
			System.out
					.println("No data to print. Count words in document with .countWords() first. ");
			// e.printStackTrace();
		}

		return this;
	}

	DocumentAnalyzer sortByWordCount(final Boolean direction) {

		List<Map.Entry<String, Integer>> list;
		try {
			list = new LinkedList<Entry<String, Integer>>(this.counter.entrySet());
			// sort list based on comparator
			Collections.sort(list, new Comparator<Object>() {
				public int compare(Object o1, Object o2) {
					int result;

					if (direction.equals(true)) {
						result = ((Comparable) ((Map.Entry) (o2)).getValue())
								.compareTo(((Map.Entry) (o1)).getValue());
					} else {
						result = ((Comparable) ((Map.Entry) (o1)).getValue())
								.compareTo(((Map.Entry) (o2)).getValue());
					}

					return result;
				}
			});

			// put sorted list into map again
			// LinkedHashMap preserves order in which keys were inserted
			Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();

			for (Iterator<Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
				Map.Entry<String, Integer> entry = it

				.next();
				sortedMap.put(entry.getKey(), entry.getValue());
			}
			this.counter = sortedMap;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out
					.println(" Null this.counter; Read text file and count words first. ");
			e.printStackTrace();
		}

		return this;
	}

	void printMostPopular(Integer count) {
		Map<String, Integer> mp = getMostPopular(count);

		System.out.printf(
				"Printing %s most popular words with their counts.. \n", count);
		String s = toString(mp);
		System.out.println(s);

	}

	String toString(Map<String, Integer> map) {
		StringWriter s = new StringWriter();

		for (Map.Entry<String, Integer> me : map.entrySet()) {
			String line = "word: " + me.getKey() + ", count: " + me.getValue()
					+ "; \n";
			s.append(line);
		}
		return s.toString();

	}

	Map<String, Integer> getMostPopular(Integer count) {
		Map<String, Integer> mp = new LinkedHashMap<>();
		int i = 0;
		for (Iterator<Entry<String, Integer>> it = this.counter.entrySet()
				.iterator(); i <= count && it.hasNext(); i++) {
			Map.Entry<String, Integer> entry = it
					.next();
			mp.put(entry.getKey(), entry.getValue());
		}

		return mp;
	}

	DocumentAnalyzer countWords() {
		this.counter = new HashMap<String, Integer>();

		try {
			for (String line : this.fileContents) {
				// process the line: split and count words:
				StringTokenizer st = new StringTokenizer(line);
				while (st.hasMoreTokens()) {
					String token = st.nextToken().toLowerCase();
					Integer tokenCounter = 0;

					if (!this.counter.containsKey(token)) {
						tokenCounter = 1;
					} else {
						tokenCounter = this.counter.get(token) + 1;
					}
					this.counter.put(token, tokenCounter);
				}
			}
		} catch (NullPointerException e) {
			System.out
					.println("Error: nothing to count. Read text file with .readTextFile() first.");
			// e.printStackTrace();
		}

		return this;
	}

	private File inputFile;
	private List<String> fileContents;
	private Map<String, Integer> counter;
}