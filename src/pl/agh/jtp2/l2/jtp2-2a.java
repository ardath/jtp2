package pl.agh.jtp2.l2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

class JTP2_2a {

	public static void main(String[] args) {

		File inputFile = new File(args[0]);

		DocumentAnalyzer da = new DocumentAnalyzer();
		da.readTextFile(inputFile);
		da.countWords();
		// da.countWords().printAllWordsWithCounts();
		// Boolean sortAscending = false;
		Boolean sortAscending = true;
		da.sortByWordCount(sortAscending);

		// da.printAllWordsWithCounts();
		String mostPopular = da.toString(da.getMostPopular(25));
		System.out.println(mostPopular);

		// save to file:
		File outputFile = new File("jtp2-2a.txt");
		// if file doesn't exists, then create it
		if (!outputFile.exists())
			try {
				outputFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		FileWriter fw;
		try {
			fw = new FileWriter(outputFile.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(mostPopular);
			bw.close();
			System.out.println("File saved. ");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}